// IMPORT REACT
import React from 'react';
import { Link } from 'react-router-dom';

// IMPORT COMPONENT
import Header from '../components/header/Header';
import Footer from '../components/footer/Footer';

// IMPORT CSS
import '../styles/main.css';
import '../styles/error.css';


// ========= FUNCTION =========
function Error() {
    return (
        <React.Fragment>
            <div className='container'>
            <Header />
            <div className='main_content'>
                <p className='number_error'>404</p>
                <p className='error_message'>Oups! La page que vous demandez n'existe pas.</p>
                <Link to="/"><p className='error_back'>Retourner sur la page d'accueil</p></Link>
            </div>
            </div>
        </React.Fragment>
    )
}

// EXPORT
export default Error
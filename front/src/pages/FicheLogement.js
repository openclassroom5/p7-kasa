// IMPORT REACT
import React from 'react';
import { useParams, Navigate } from 'react-router-dom';

// IMPORT COMPONENTS
import Header from '../components/header/Header.js';
import Footer from '../components/footer/Footer.js';
import Carousel from '../components/carousel/Carousel';
import Info from '../components/infos/Info.js';
import Dropdown from '../components/dropdown/Dropdown.js';

// IMPORT CSS
import '../styles/main.css';

// IMPORT DATA
import { appartements } from '../data/locations.js';

// === FUNCTION ===
function FicheLogement() {


// Get ID from ROUTER in ' Index.js 'and Check 
    const { id } = useParams();
    const appartement = appartements.find(appartement => appartement.id === id)


// If Nothing then => 404 Error
    if (appartement === undefined) {
        return <Navigate replace to="404" />;
    }

// Const needed cause both dropdown have different name, content and display.
    const infoDropdown = [
        {
            title: "Description",
            content: appartement.description,
            isList: false
        },
        {
            title: "Equipements",
            content: appartement.equipments,
            isList: true
        }
    ]


// ==================== HOUSE PAGE ==================== //
// == Display of choose Appart by user 
// == Return : Header, Carouselle, Info, loop on infoDropdown for Dropdown and Footer comp inside a Fragment
//     Carouselle comp props send :
//         pictures => array of pictures to display
//         title => needed for the alt
//     Info comp props send :
//         title, location, ownerName, ownerImg => data to display
//         tags => array of tags
//         rating => number between 0 and 5
// ==================================================== //


    return (
        <React.Fragment>
            <div className='container'>
            <Header />
            <div className="main_content">
                <Carousel pictures={appartement.pictures} title={appartement.title}/>
                <Info
                title={appartement.title}
                location={appartement.location}
                ownerName={appartement.host.name}
                ownerImg={appartement.host.picture}
                tags={appartement.tags}
                rating={appartement.rating} />
                <div className='dropdown_fiche'>
                    {infoDropdown.map((info) => {
                        return <Dropdown key={info.title} title={info.title} content={info.content} isList={info.isList} classNameDescription="dropdown_description_fiche" classNameDisplay="dropdown_display_fiche" />
                    })}
                </div>
            </div>
            </div>
            <Footer />
        </React.Fragment>
    )
}

// EXPORT
export default FicheLogement
// ========= IMPORT ========= //

// IMPORT REACT
import React from 'react';

// IMPORT COMPONENTS
import Header from '../components/header/Header';
import Footer from '../components/footer/Footer';
import Background from '../components/background/Background'
import Thumb from '../components/thumbnail/Thumb' 

// IMPORT CSS
import '../styles/main.css';
import BackgroundHome from '../assets/BackgroundHome.jpg'


// ==================== HOME PAGE ==================== //
// == Return : Header, Background, Thumbnail and Footer
// == Background comp props send :
//     classname => used in "A-propos.js" too with slight difference
//     Img => background image
//     Title => only needed for home page. Text in-front
// =================================================== //


// === FUNCTION ===
function App() {
  return (
    <React.Fragment>
      <div className='container'>
      <Header classNameNavHome="underline" />
      <div className='main_content_home'>
        <Background className="home_img" img={BackgroundHome} title="Chez vous, partout et ailleurs" />
        <Thumb />
      </div>
      </div>
      <Footer />
    </React.Fragment>
  )
}

export default App;
// ========= IMPORT ========= //

// IMPORT REACT
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// IMPORT CSS 
import './styles/index.css';

// IMPORT PAGES
import Main from './pages/Main';
import Error from './pages/Error';
import FicheLogement from './pages/FicheLogement';
import Propos from './pages/A-propos';

// IMPORT COMPONENTS 
import Appartement from './components/houses/Appart.js';


// ========= ROOT ========= //

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route exact path='/' element={<Main />}></Route>
        <Route path="FicheLogement/:id" component={Appartement} element={<FicheLogement />}></Route>
        <Route path="A-propos" element={<Propos />}></Route>
        <Route path="*" element={<Error />}></Route>
        
      </Routes>
    </Router>
  </React.StrictMode>
);
// IMPORT CSS
import '../thumbnail/thumb.css';

// IMPORT COMPONENT
import { appartements } from '../../data/locations.js'
import Appartement from '../houses/Appart'

// === FUNCTION ===
function Thumb() {

    return (
        <div className='house_list'>
  {appartements.map((appartement) => (
  <Appartement key={appartement.id} appartement={appartement}/>
            ))}
        </div>
    )
}


// EXPORT
export default Thumb
// IMPORT CSS
import '../thumbnail/thumb.css'

// IMPORT REACT
import { Link } from 'react-router-dom'


// === FUNCTION ===
function Appartement(props) {

    // IMPORT PROPS
const {title, id} = props.appartement

    return (
        <Link to={`/FicheLogement/${id}`} className="thumb" style={{backgroundImage: `url(${props.appartement.cover})`}}>
            <p className="thumb_title">{title}</p>
        </Link>
    )
}

// EXPORT
export default Appartement 

// IMPORT CSS
import './header.css';
import Logo from '../../assets/LOGO.svg';

// IMPORT REACT
import { Link } from 'react-router-dom';

// === FUNCTION ===
function Header(props) {
    return (
        <div className="navbar" >
            <div className="logo">
                <img src={Logo} alt="Logo Kasa" />
            </div>
            <div className="nav_menu">
                <Link className={props.classNameNavHome} to="/">Accueil</Link>
                <Link className={props.classNameNavAbout} to="/A-propos">A propos</Link>
            </div>
        </div>
    )
}

// EXPORT
export default Header
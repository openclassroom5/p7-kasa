// IMPORT REACT
import { useState } from 'react';

// IMPORT CSS
import '../dropdown/dropdown.css';

// === FUNCTION ===
function Dropdown(props) {
    const [open, isOpen] = useState(false)

    return(
        (props.isList
            ?   <div className={props.classNameDisplay} onClick={() => isOpen(!open)}>
                    {open
                    ?   <div><div className={props.classNameDescription} key={props.title}>
                            <p className='dropdown_title'>{props.title}</p>
                            <i className="fa-solid fa-chevron-up"></i>
                        </div>
                        <div className='dropdown_content'>
                            {props.content.map((content) => {
                                return <p key={content}>{content}</p>
                            })}
                        </div></div>
                    :   <div className={props.classNameDescription} key={props.title}>
                            <p className='dropdown_title'>{props.title}</p>
                            <i className="fa-solid fa-chevron-down"></i>
                        </div> }
                </div>
            :
        <div className={props.classNameDisplay} onClick={() => isOpen(!open)}>
            {open
            ?   <div className='dropdown_container'>
                    <div className={props.classNameDescription} key={props.title} >
                        <p className='dropdown_title'>{props.title}</p>
                        <div className='dropdown_icon'><i className="fa-solid fa-chevron-up"></i>
                    </div>
                </div>
            <p className='dropdown_content'>{props.content}</p></div>
            :   <div className={props.classNameDescription} key={props.title} >
                    <p className='dropdown_title'>{props.title}</p>
                    <div className='dropdown_icon'><i className="fa-solid fa-chevron-down"></i></div>
                </div>
            }
        </div>
    ))
}


// EXPORT
export default Dropdown
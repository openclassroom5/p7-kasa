// IMPORT CSS
import './footer.css'
import logoFooter from '../../assets/LOGO_FOOTER.svg'

// === FUNCTION ===
function Footer() {
    return (
        <div className="footer">
            <img src={logoFooter} alt="Kasa Logo Footer" />
            <p>© 2020 Kasa. All rights reserved</p>
        </div>
    )
}

// EXPORT
export default Footer
// IMPORT CSS
import './background.css'

// === FUNCTION === 
function Background(props) {
    console.log(props.img);
    return (
        
        <div className={props.className}> 
                <img className="background_img" src={props.img} alt="background" />
            <h2 className="home_title">{props.title}</h2>
        </div>
    )
}
 


// EXPORT
export default Background
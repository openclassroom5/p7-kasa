
// === FUNCTION === 

function Tag(props) {
  const tags = props.tags
  return (

// ========= RATING ========= //
// Loop pour chaque Tag
// Tag props :
//   Key => tag, since each tag is unique and only used once.
// ========================== //


    <div className='info_tag'>
        {tags.map((tag) => {
           return <p className='tag' key={tag}>{tag}</p>
        })}
    </div>
  )
}

// EXPORT
export default Tag